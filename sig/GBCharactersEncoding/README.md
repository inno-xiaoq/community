# GBCharactersEncoding SIG组

## 工作目标

&emsp;&emsp; GBCharactersEncoding sig组旨在跟踪、研究和推动国家编码标准的发展，提供国家编码标准的基础实现库，实现国家编码标准的广泛应用和落地
- 跟踪、研究国家编码标准，研究落地实现中的各种问题
- 为推广国家编码标准的落地，提供和维护国家编码标准的基础实现库
- 推动国内国际交流，实现国家编码标准在开源社区的广泛应用和落地，争取成为国家编码标准的权威实现组织

&emsp;&emsp; GBCharactersEncoding sig组欢迎广大社区爱好者参与，并向我们提出问题和建议。只要您对操作系统开发和国家标准感兴趣，都可以加入我们sig组。我们相信，在您的参与和支持下，GBCharactersEncoding sig组一定能够发挥更大的作用，为汉字编码标准在Linux生态上的推广做出更多贡献。
## SIG成员
- hanteng@kylinos.cn
- liulinsong@kylinos.cn
- linyuxuan@kylinos.cn
- zouxiaoyi@kylinos.cn
### SIG维护包列表
- harfbuzz
- icu
- gbcharactersencoding

