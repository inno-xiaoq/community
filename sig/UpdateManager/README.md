## 系统升级SIG组

## 工作目标

致力于组建系统升级开源社区，负责开发和维护OpenKylin系统升级机制

## SIG成员

owner

- 张东伟`zhangdongwei@kylinos.cn`
- 杨文轩`yangwenxuan@kylinos.cn`

maintainer

- 王松`wangsong@kylinos.cn`
- 沈亚峰`shenyafeng@kylinos.cn`
- 何承原`hechengyuan@kylinos.cn`
- 罗学毅`luoxueyi@kylinos.cn`

## SIG维护包列表

- kylin-system-updater
- kylin-update-frontend


## SIG邮件列表
updatemanager@lists.openkylin.top
